const CONFIG = {
  API_URL: "https://api.playdeviant.com",
  WS_API_URL: "wss://api.playdeviant.com"
}

export default CONFIG