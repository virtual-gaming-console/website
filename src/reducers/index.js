import { combineReducers } from "redux"
import { lobby } from './lobby'
import { cursor } from './cursor'
import { auth } from './auth'

export const rootReducer = combineReducers({
  lobby,
  cursor,
  auth
})