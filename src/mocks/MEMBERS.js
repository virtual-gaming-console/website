const MEMBERS = [
  {
    id: 1,
    nickname: 'Slace',
    picture: 'https://static-cdn.jtvnw.net/jtv_user_pictures/0492e1f5-0c02-4599-a2b5-b8f6faccd444-profile_image-70x70.png',
    isLobbyLeader: true
  },
  {
    id: 2,
    nickname: 'Aezie',
    picture: 'https://cdn.discordapp.com/avatars/141283165636526081/6b82bb5418d0a46d1ee81f44f866a46f.png?size=128',
    isLobbyLeader: false
  },
  {
    id: 3,
    nickname: 'Ophyr',
    picture: 'https://cdn.discordapp.com/avatars/141185150691049473/5356a231c70e72212c74b6cd4ca55979.png?size=128',
    isLobbyLeader: false
  }
]

export default MEMBERS