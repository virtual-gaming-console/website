import { cursorConstants } from "../constants"

export const updateCursor = cursor => ({
  type: cursorConstants.CURSOR_UPDATE,
  cursor
})